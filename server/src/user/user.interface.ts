import { Document } from 'mongoose';

export interface IUser {
    readonly id: string;
    firstName: string;
    lastName: string;
    email: string;
    login: string;
    authorities: string[];
    resetDate: Date;
    activationKey: string;
    activated: boolean;
    resetKey: string;
    password: string;
}

export class User extends Document implements IUser {
    readonly id: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly login: string;
    password: string;
    readonly authorities: string[];
    readonly resetDate: Date;
    readonly activationKey: string;
    readonly activated: boolean;
    readonly resetKey: string;
}

