import { Module } from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import { UserModule } from './user/user.module';
import { AccountModule } from './account/account.module';
import { AuthModule } from './auth/auth.module';
import {MailerModule} from '@nestjs-modules/mailer';
import { SharedModule } from './shared/shared.module';
import { EntityModule } from './entity/entity.module';
import {MAIL_CONFIG} from './shared/config/mail/node-mailer.config';
import {MongooseModule} from '@nestjs/mongoose';




@Module({
  imports: [
      ConfigModule.forRoot(),
      MongooseModule.forRoot(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }),
      MailerModule.forRoot(MAIL_CONFIG),
      UserModule,
      AccountModule,
      EntityModule,
      AuthModule,
      SharedModule,
  ],
})
export class AppModule {}
