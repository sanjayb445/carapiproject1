import { Document } from 'mongoose';

export interface IVarient {
    id?: string;

    brandid?: string;

    modelid?: string;

    carname?: string;

    carimg?: string;

    fueltype?: string;

    transmission?: string;

    price?: string;

    enginecapacity?: string;

    enginetype?: string;

    colorurl?: string;

    colorname?: string;

}

export class Varient extends Document implements IVarient {
    id: string;

    brandid: string;

    modelid: string;

    carname: string;

    carimg: string;

    fueltype: string;

    transmission: string;

    price: string;

    enginecapacity: string;

    enginetype: string;

    colorurl: string;

    colorname: string;

}


