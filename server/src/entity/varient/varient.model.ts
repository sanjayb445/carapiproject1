import * as mongoose from 'mongoose';

export const VarientSchema = new mongoose.Schema({

    brandid: {type: mongoose.SchemaTypes.String, required: false},

    modelid: {type: mongoose.SchemaTypes.String, required: false},

    carname: {type: mongoose.SchemaTypes.String, required: false},

    carimg: {type: mongoose.SchemaTypes.String, required: false},

    fueltype: {type: mongoose.SchemaTypes.String, required: false},

    transmission: {type: mongoose.SchemaTypes.String, required: false},

    price: {type: mongoose.SchemaTypes.String, required: false},

    enginecapacity: {type: mongoose.SchemaTypes.String, required: false},

    enginetype: {type: mongoose.SchemaTypes.String, required: false},

    colorurl: {type: mongoose.SchemaTypes.String, required: false},

    colorname: {type: mongoose.SchemaTypes.String, required: false},

});



