import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {VarientSchema} from './varient.model';
import {VarientService} from './varient.service';
import {VarientRepositroy} from './varient.repository';
import {VarientController} from './varient.controller';


@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Varient', schema: VarientSchema}]),
  ],
  providers: [VarientService, VarientRepositroy],
  controllers: [VarientController],
  exports: [VarientService, VarientRepositroy],
})
export class VarientModule {}
