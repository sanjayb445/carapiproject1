import {Injectable} from '@nestjs/common';
import {IVarient} from './varient.interface';
import {VarientDTO} from './varient.DTO';

@Injectable()
export class VarientMapper {

  static mapVarientToVarientDTO(varient: IVarient): VarientDTO {
    return {
      id: varient.id,
      
      brandid: varient.brandid,
      
      modelid: varient.modelid,
      
      carname: varient.carname,
      
      carimg: varient.carimg,
      
      fueltype: varient.fueltype,
      
      transmission: varient.transmission,
      
      price: varient.price,
      
      enginecapacity: varient.enginecapacity,
      
      enginetype: varient.enginetype,
      
      colorurl: varient.colorurl,
      
      colorname: varient.colorname,
      
    };
  }

  static mapVarientDTOToVarient(varientDTO: VarientDTO): IVarient {
    return {
      id: varientDTO.id,
      
      brandid: varientDTO.brandid,
      
      modelid: varientDTO.modelid,
      
      carname: varientDTO.carname,
      
      carimg: varientDTO.carimg,
      
      fueltype: varientDTO.fueltype,
      
      transmission: varientDTO.transmission,
      
      price: varientDTO.price,
      
      enginecapacity: varientDTO.enginecapacity,
      
      enginetype: varientDTO.enginetype,
      
      colorurl: varientDTO.colorurl,
      
      colorname: varientDTO.colorname,
      
    };
  }

  static mapVarientListToVarientDTOList(varientList: IVarient[]): VarientDTO[] {
    const varientDTOList: VarientDTO[] = [];
    for (const varient of varientList) {
      const varientDTO = VarientMapper.mapVarientToVarientDTO(varient);
      varientDTOList.push(varientDTO);
    }
    return varientDTOList;
  }
}
