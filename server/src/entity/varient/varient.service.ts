import {VarientDTO} from './varient.DTO';
import {Injectable} from '@nestjs/common';
import {VarientRepositroy} from './varient.repository';
import {VarientMapper} from './varient.mapper';
import {IVarient} from './varient.interface';

@Injectable()
export class VarientService {
constructor(private varientRepositroy: VarientRepositroy) {
}

    async save(varientDTO: VarientDTO): Promise<VarientDTO> {
        const varient: IVarient = VarientMapper.mapVarientDTOToVarient(varientDTO);
        const result = await this.varientRepositroy.save(varient);
        return VarientMapper.mapVarientToVarientDTO(result);
    }

    async update(varientDTO: VarientDTO): Promise<VarientDTO> {
        const varient: IVarient = VarientMapper.mapVarientDTOToVarient(varientDTO);
        const result = await this.varientRepositroy.update(varient);
        return VarientMapper.mapVarientToVarientDTO(result);
    }

    async findAll(): Promise<VarientDTO[]> {
        const varientList: IVarient[] = await this.varientRepositroy.findAll();
        return VarientMapper.mapVarientListToVarientDTOList(varientList);
    }

    async findById(id: string): Promise<VarientDTO> {
        const varient: IVarient = await this.varientRepositroy.findById(id);
        return VarientMapper.mapVarientToVarientDTO(varient);
    }

    async deleteById(id: string): Promise<void> {
        return await this.varientRepositroy.deleteById(id);
    }
}
