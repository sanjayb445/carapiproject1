import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Varient, IVarient} from './varient.interface';

@Injectable()
export class VarientRepositroy {
    constructor(
        @InjectModel('Varient') private readonly varientModel: Model<Varient>,
           ) { }

    async save(varient: IVarient): Promise<IVarient> {
        const newVarient = new this.varientModel(varient);
        return newVarient.save();
    }

    async update(varient: IVarient): Promise<IVarient> {
        return await this.varientModel.findByIdAndUpdate(varient.id, varient , {new: true});
    }

    async findAll(): Promise<IVarient[]> {
        return await this.varientModel.find();
    }

    async findById(id: string): Promise<IVarient> {
        return await this.varientModel.findById(id);
    }

    async deleteById(id: string): Promise<void> {
        await this.varientModel.findByIdAndDelete(id);
    }
}
