import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {VarientService} from './varient.service';
import {VarientDTO} from './varient.DTO';
 import * as mongoose from 'mongoose'; 


@Controller('api')
export class VarientController {

    constructor(private varientService: VarientService) {
    }

    @Post('varients')
    async create(@Body() varientDTO: VarientDTO): Promise<VarientDTO> {
        if (varientDTO.id) {
        throw new BadRequestException('A new varient cannot already have an Id');
        }
        return await this.varientService.save(varientDTO);
    }

    @Put('varients')
    async update(@Body() varientDTO: VarientDTO): Promise<VarientDTO> {
        if (!varientDTO.id) {
            throw new BadRequestException('A varient must have an Id');
        }if (varientDTO.id && !mongoose.Types.ObjectId.isValid(varientDTO.id)) {
                throw new BadRequestException('Invalid Id.');
            }
        return await this.varientService.update(varientDTO);
    }

    @Get('varients')
    async getAll(): Promise<VarientDTO[]> {
        return await this.varientService.findAll();
    }

    @Get('varients/:id')
    async getOne(@Param('id') id: string): Promise<VarientDTO> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.varientService.findById(id);
    }

    @Delete('varients/:id')
    async delete(@Param('id') id: string): Promise<void> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.varientService.deleteById(id);
    }
}
