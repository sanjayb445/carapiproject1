export class VarientDTO {
    id?: string;

    brandid?: string;

    modelid?: string;

    carname?: string;

    carimg?: string;

    fueltype?: string;

    transmission?: string;

    price?: string;

    enginecapacity?: string;

    enginetype?: string;

    colorurl?: string;

    colorname?: string;

}


