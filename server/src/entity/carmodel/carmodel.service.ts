import {CarmodelDTO} from './carmodel.DTO';
import {Injectable} from '@nestjs/common';
import {CarmodelRepositroy} from './carmodel.repository';
import {CarmodelMapper} from './carmodel.mapper';
import {ICarmodel} from './carmodel.interface';

@Injectable()
export class CarmodelService {
constructor(private carmodelRepositroy: CarmodelRepositroy) {
}

    async save(carmodelDTO: CarmodelDTO): Promise<CarmodelDTO> {
        const carmodel: ICarmodel = CarmodelMapper.mapCarmodelDTOToCarmodel(carmodelDTO);
        const result = await this.carmodelRepositroy.save(carmodel);
        return CarmodelMapper.mapCarmodelToCarmodelDTO(result);
    }

    async update(carmodelDTO: CarmodelDTO): Promise<CarmodelDTO> {
        const carmodel: ICarmodel = CarmodelMapper.mapCarmodelDTOToCarmodel(carmodelDTO);
        const result = await this.carmodelRepositroy.update(carmodel);
        return CarmodelMapper.mapCarmodelToCarmodelDTO(result);
    }

    async findAll(): Promise<CarmodelDTO[]> {
        const carmodelList: ICarmodel[] = await this.carmodelRepositroy.findAll();
        return CarmodelMapper.mapCarmodelListToCarmodelDTOList(carmodelList);
    }

    async findById(id: string): Promise<CarmodelDTO> {
        const carmodel: ICarmodel = await this.carmodelRepositroy.findById(id);
        return CarmodelMapper.mapCarmodelToCarmodelDTO(carmodel);
    }

    async deleteById(id: string): Promise<void> {
        return await this.carmodelRepositroy.deleteById(id);
    }
}
