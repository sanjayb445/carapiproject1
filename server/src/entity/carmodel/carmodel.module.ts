import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {CarmodelSchema} from './carmodel.model';
import {CarmodelService} from './carmodel.service';
import {CarmodelRepositroy} from './carmodel.repository';
import {CarmodelController} from './carmodel.controller';


@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Carmodel', schema: CarmodelSchema}]),
  ],
  providers: [CarmodelService, CarmodelRepositroy],
  controllers: [CarmodelController],
  exports: [CarmodelService, CarmodelRepositroy],
})
export class CarmodelModule {}
