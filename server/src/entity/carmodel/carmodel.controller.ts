import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {CarmodelService} from './carmodel.service';
import {CarmodelDTO} from './carmodel.DTO';
 import * as mongoose from 'mongoose'; 


@Controller('api')
export class CarmodelController {

    constructor(private carmodelService: CarmodelService) {
    }

    @Post('carmodels')
    async create(@Body() carmodelDTO: CarmodelDTO): Promise<CarmodelDTO> {
        if (carmodelDTO.id) {
        throw new BadRequestException('A new carmodel cannot already have an Id');
        }
        return await this.carmodelService.save(carmodelDTO);
    }

    @Put('carmodels')
    async update(@Body() carmodelDTO: CarmodelDTO): Promise<CarmodelDTO> {
        if (!carmodelDTO.id) {
            throw new BadRequestException('A carmodel must have an Id');
        }if (carmodelDTO.id && !mongoose.Types.ObjectId.isValid(carmodelDTO.id)) {
                throw new BadRequestException('Invalid Id.');
            }
        return await this.carmodelService.update(carmodelDTO);
    }

    @Get('carmodels')
    async getAll(): Promise<CarmodelDTO[]> {
        return await this.carmodelService.findAll();
    }

    @Get('carmodels/:id')
    async getOne(@Param('id') id: string): Promise<CarmodelDTO> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.carmodelService.findById(id);
    }

    @Delete('carmodels/:id')
    async delete(@Param('id') id: string): Promise<void> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.carmodelService.deleteById(id);
    }
}
