import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Carmodel, ICarmodel} from './carmodel.interface';

@Injectable()
export class CarmodelRepositroy {
    constructor(
        @InjectModel('Carmodel') private readonly carmodelModel: Model<Carmodel>,
           ) { }

    async save(carmodel: ICarmodel): Promise<ICarmodel> {
        const newCarmodel = new this.carmodelModel(carmodel);
        return newCarmodel.save();
    }

    async update(carmodel: ICarmodel): Promise<ICarmodel> {
        return await this.carmodelModel.findByIdAndUpdate(carmodel.id, carmodel , {new: true});
    }

    async findAll(): Promise<ICarmodel[]> {
        return await this.carmodelModel.find();
    }

    async findById(id: string): Promise<ICarmodel> {
        return await this.carmodelModel.findById(id);
    }

    async deleteById(id: string): Promise<void> {
        await this.carmodelModel.findByIdAndDelete(id);
    }
}
