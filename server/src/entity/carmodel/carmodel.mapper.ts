import {Injectable} from '@nestjs/common';
import {ICarmodel} from './carmodel.interface';
import {CarmodelDTO} from './carmodel.DTO';

@Injectable()
export class CarmodelMapper {

  static mapCarmodelToCarmodelDTO(carmodel: ICarmodel): CarmodelDTO {
    return {
      id: carmodel.id,
      
      brandid: carmodel.brandid,
      
      modelimg: carmodel.modelimg,
      
      modelname: carmodel.modelname,
      
      varient: carmodel.varient,
      
    };
  }

  static mapCarmodelDTOToCarmodel(carmodelDTO: CarmodelDTO): ICarmodel {
    return {
      id: carmodelDTO.id,
      
      brandid: carmodelDTO.brandid,
      
      modelimg: carmodelDTO.modelimg,
      
      modelname: carmodelDTO.modelname,
      
      varient: carmodelDTO.varient,
      
    };
  }

  static mapCarmodelListToCarmodelDTOList(carmodelList: ICarmodel[]): CarmodelDTO[] {
    const carmodelDTOList: CarmodelDTO[] = [];
    for (const carmodel of carmodelList) {
      const carmodelDTO = CarmodelMapper.mapCarmodelToCarmodelDTO(carmodel);
      carmodelDTOList.push(carmodelDTO);
    }
    return carmodelDTOList;
  }
}
