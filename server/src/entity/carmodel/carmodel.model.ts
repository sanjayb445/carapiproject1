import * as mongoose from 'mongoose';

export const CarmodelSchema = new mongoose.Schema({

    brandid: {type: mongoose.SchemaTypes.String, required: false},

    modelimg: {type: mongoose.SchemaTypes.String, required: false},

    modelname: {type: mongoose.SchemaTypes.String, required: false},

    varient: {type: mongoose.SchemaTypes.String, required: false},

});



