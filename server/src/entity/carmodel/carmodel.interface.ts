import { Document } from 'mongoose';

export interface ICarmodel {
    id?: string;

    brandid?: string;

    modelimg?: string;

    modelname?: string;

    varient?: string;

}

export class Carmodel extends Document implements ICarmodel {
    id: string;

    brandid: string;

    modelimg: string;

    modelname: string;

    varient: string;

}


