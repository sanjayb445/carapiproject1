import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {StateService} from './state.service';
import {StateDTO} from './state.DTO';
 import * as mongoose from 'mongoose'; 


@Controller('api')
export class StateController {

    constructor(private stateService: StateService) {
    }

    @Post('states')
    async create(@Body() stateDTO: StateDTO): Promise<StateDTO> {
        if (stateDTO.id) {
        throw new BadRequestException('A new state cannot already have an Id');
        }
        return await this.stateService.save(stateDTO);
    }

    @Put('states')
    async update(@Body() stateDTO: StateDTO): Promise<StateDTO> {
        if (!stateDTO.id) {
            throw new BadRequestException('A state must have an Id');
        }if (stateDTO.id && !mongoose.Types.ObjectId.isValid(stateDTO.id)) {
                throw new BadRequestException('Invalid Id.');
            }
        return await this.stateService.update(stateDTO);
    }

    @Get('states')
    async getAll(): Promise<StateDTO[]> {
        return await this.stateService.findAll();
    }

    @Get('states/:id')
    async getOne(@Param('id') id: string): Promise<StateDTO> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.stateService.findById(id);
    }

    @Delete('states/:id')
    async delete(@Param('id') id: string): Promise<void> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.stateService.deleteById(id);
    }
}
