import {Injectable} from '@nestjs/common';
import {IState} from './state.interface';
import {StateDTO} from './state.DTO';

@Injectable()
export class StateMapper {

  static mapStateToStateDTO(state: IState): StateDTO {
    return {
      id: state.id,
      
      statename: state.statename,
      
      isleading: state.isleading,
      
    };
  }

  static mapStateDTOToState(stateDTO: StateDTO): IState {
    return {
      id: stateDTO.id,
      
      statename: stateDTO.statename,
      
      isleading: stateDTO.isleading,
      
    };
  }

  static mapStateListToStateDTOList(stateList: IState[]): StateDTO[] {
    const stateDTOList: StateDTO[] = [];
    for (const state of stateList) {
      const stateDTO = StateMapper.mapStateToStateDTO(state);
      stateDTOList.push(stateDTO);
    }
    return stateDTOList;
  }
}
