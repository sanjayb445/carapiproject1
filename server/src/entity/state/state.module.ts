import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {StateSchema} from './state.model';
import {StateService} from './state.service';
import {StateRepositroy} from './state.repository';
import {StateController} from './state.controller';


@Module({
  imports: [
    MongooseModule.forFeature([{name: 'State', schema: StateSchema}]),
  ],
  providers: [StateService, StateRepositroy],
  controllers: [StateController],
  exports: [StateService, StateRepositroy],
})
export class StateModule {}
