import { Document } from 'mongoose';

export interface IState {
    id?: string;

    statename?: string;

    isleading?: string;

}

export class State extends Document implements IState {
    id: string;

    statename: string;

    isleading: string;

}


