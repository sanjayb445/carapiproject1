import {StateDTO} from './state.DTO';
import {Injectable} from '@nestjs/common';
import {StateRepositroy} from './state.repository';
import {StateMapper} from './state.mapper';
import {IState} from './state.interface';

@Injectable()
export class StateService {
constructor(private stateRepositroy: StateRepositroy) {
}

    async save(stateDTO: StateDTO): Promise<StateDTO> {
        const state: IState = StateMapper.mapStateDTOToState(stateDTO);
        const result = await this.stateRepositroy.save(state);
        return StateMapper.mapStateToStateDTO(result);
    }

    async update(stateDTO: StateDTO): Promise<StateDTO> {
        const state: IState = StateMapper.mapStateDTOToState(stateDTO);
        const result = await this.stateRepositroy.update(state);
        return StateMapper.mapStateToStateDTO(result);
    }

    async findAll(): Promise<StateDTO[]> {
        const stateList: IState[] = await this.stateRepositroy.findAll();
        return StateMapper.mapStateListToStateDTOList(stateList);
    }

    async findById(id: string): Promise<StateDTO> {
        const state: IState = await this.stateRepositroy.findById(id);
        return StateMapper.mapStateToStateDTO(state);
    }

    async deleteById(id: string): Promise<void> {
        return await this.stateRepositroy.deleteById(id);
    }
}
