import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {State, IState} from './state.interface';

@Injectable()
export class StateRepositroy {
    constructor(
        @InjectModel('State') private readonly stateModel: Model<State>,
           ) { }

    async save(state: IState): Promise<IState> {
        const newState = new this.stateModel(state);
        return newState.save();
    }

    async update(state: IState): Promise<IState> {
        return await this.stateModel.findByIdAndUpdate(state.id, state , {new: true});
    }

    async findAll(): Promise<IState[]> {
        return await this.stateModel.find();
    }

    async findById(id: string): Promise<IState> {
        return await this.stateModel.findById(id);
    }

    async deleteById(id: string): Promise<void> {
        await this.stateModel.findByIdAndDelete(id);
    }
}
