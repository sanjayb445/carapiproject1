import {Module} from '@nestjs/common';

import {BrandModule} from './brand/brand.module';
import {VarientModule} from './varient/varient.module';
import {CityModule} from './city/city.module';
import {StateModule} from './state/state.module';
import {CarmodelModule} from './carmodel/carmodel.module';
// Flare writing content --- flare will use it to inject module paths

@Module({
  imports: [
BrandModule,
VarientModule,
CityModule,
StateModule,
CarmodelModule,
// Flare writing content --- flare will use it to inject modules
  ],
})
export class EntityModule {
}
