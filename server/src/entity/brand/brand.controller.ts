import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {BrandService} from './brand.service';
import {BrandDTO} from './brand.DTO';
 import * as mongoose from 'mongoose'; 


@Controller('api')
export class BrandController {

    constructor(private brandService: BrandService) {
    }

    @Post('brands')
    async create(@Body() brandDTO: BrandDTO): Promise<BrandDTO> {
        if (brandDTO.id) {
        throw new BadRequestException('A new brand cannot already have an Id');
        }
        return await this.brandService.save(brandDTO);
    }

    @Put('brands')
    async update(@Body() brandDTO: BrandDTO): Promise<BrandDTO> {
        if (!brandDTO.id) {
            throw new BadRequestException('A brand must have an Id');
        }if (brandDTO.id && !mongoose.Types.ObjectId.isValid(brandDTO.id)) {
                throw new BadRequestException('Invalid Id.');
            }
        return await this.brandService.update(brandDTO);
    }

    @Get('brands')
    async getAll(): Promise<BrandDTO[]> {
        return await this.brandService.findAll();
    }

    @Get('brands/:id')
    async getOne(@Param('id') id: string): Promise<BrandDTO> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.brandService.findById(id);
    }

    @Delete('brands/:id')
    async delete(@Param('id') id: string): Promise<void> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.brandService.deleteById(id);
    }
    @Get('brands/brand:brandid')
    async getAllBrand(@Param('brandid')brandid:string): Promise<BrandDTO[]> {
        return await this.brandService.findAllBrand(brandid);
    }
}
