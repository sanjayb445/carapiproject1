import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {BrandSchema} from './brand.model';
import {BrandService} from './brand.service';
import {BrandRepositroy} from './brand.repository';
import {BrandController} from './brand.controller';


@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Brand', schema: BrandSchema}]),
  ],
  providers: [BrandService, BrandRepositroy],
  controllers: [BrandController],
  exports: [BrandService, BrandRepositroy],
})
export class BrandModule {}
