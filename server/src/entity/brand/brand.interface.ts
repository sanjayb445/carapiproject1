import { Document } from 'mongoose';

export interface IBrand {
    id?: string;

    brandllogo?: string;

    brandname?: string;

}

export class Brand extends Document implements IBrand {
    id: string;

    brandllogo: string;

    brandname: string;

}


