import {Injectable} from '@nestjs/common';
import {IBrand} from './brand.interface';
import {BrandDTO} from './brand.DTO';

@Injectable()
export class BrandMapper {

  static mapBrandToBrandDTO(brand: IBrand): BrandDTO {
    return {
      id: brand.id,
      
      brandllogo: brand.brandllogo,
      
      brandname: brand.brandname,
      
    };
  }

  static mapBrandDTOToBrand(brandDTO: BrandDTO): IBrand {
    return {
      id: brandDTO.id,
      
      brandllogo: brandDTO.brandllogo,
      
      brandname: brandDTO.brandname,
      
    };
  }

  static mapBrandListToBrandDTOList(brandList: IBrand[]): BrandDTO[] {
    const brandDTOList: BrandDTO[] = [];
    for (const brand of brandList) {
      const brandDTO = BrandMapper.mapBrandToBrandDTO(brand);
      brandDTOList.push(brandDTO);
    }
    return brandDTOList;
  }
}
