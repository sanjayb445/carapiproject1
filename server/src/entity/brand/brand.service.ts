import {BrandDTO} from './brand.DTO';
import {Injectable} from '@nestjs/common';
import {BrandRepositroy} from './brand.repository';
import {BrandMapper} from './brand.mapper';
import {IBrand} from './brand.interface';

@Injectable()
export class BrandService {
constructor(private brandRepositroy: BrandRepositroy) {
}

    async save(brandDTO: BrandDTO): Promise<BrandDTO> {
        const brand: IBrand = BrandMapper.mapBrandDTOToBrand(brandDTO);
        const result = await this.brandRepositroy.save(brand);
        return BrandMapper.mapBrandToBrandDTO(result);
    }

    async update(brandDTO: BrandDTO): Promise<BrandDTO> {
        const brand: IBrand = BrandMapper.mapBrandDTOToBrand(brandDTO);
        const result = await this.brandRepositroy.update(brand);
        return BrandMapper.mapBrandToBrandDTO(result);
    }

    async findAll(): Promise<BrandDTO[]> {
        const brandList: IBrand[] = await this.brandRepositroy.findAll();
        return BrandMapper.mapBrandListToBrandDTOList(brandList);
    }

    async findById(id: string): Promise<BrandDTO> {
        const brand: IBrand = await this.brandRepositroy.findById(id);
        return BrandMapper.mapBrandToBrandDTO(brand);
    }

    async deleteById(id: string): Promise<void> {
        return await this.brandRepositroy.deleteById(id);
    }
    async findAllBrand(brandid:string): Promise<BrandDTO[]> {
        const brandList: IBrand[] = await this.brandRepositroy.findAllBrand(brandid);
        return BrandMapper.mapBrandListToBrandDTOList(brandList);
    }
}
