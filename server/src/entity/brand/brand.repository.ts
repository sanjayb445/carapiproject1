import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Brand, IBrand} from './brand.interface';

@Injectable()
export class BrandRepositroy {
    constructor(
        @InjectModel('Brand') private readonly brandModel: Model<Brand>,
           ) { }

    async save(brand: IBrand): Promise<IBrand> {
        const newBrand = new this.brandModel(brand);
        return newBrand.save();
    }

    async update(brand: IBrand): Promise<IBrand> {
        return await this.brandModel.findByIdAndUpdate(brand.id, brand , {new: true});
    }

    async findAll(): Promise<IBrand[]> {
        return await this.brandModel.find();
    }

    async findById(id: string): Promise<IBrand> {
        return await this.brandModel.findById(id);
    }

    async deleteById(id: string): Promise<void> {
        await this.brandModel.findByIdAndDelete(id);
    }
    async findAllBrand(brandid:string): Promise<IBrand[]> {
        return await this.brandModel.find({brandid:brandid}).populate('brandid');
    }

}
