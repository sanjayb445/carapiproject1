import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {City, ICity} from './city.interface';

@Injectable()
export class CityRepositroy {
    constructor(
        @InjectModel('City') private readonly cityModel: Model<City>,
           ) { }

    async save(city: ICity): Promise<ICity> {
        const newCity = new this.cityModel(city);
        return newCity.save();
    }

    async update(city: ICity): Promise<ICity> {
        return await this.cityModel.findByIdAndUpdate(city.id, city , {new: true});
    }

    async findAll(): Promise<ICity[]> {
        return await this.cityModel.find();
    }

    async findById(id: string): Promise<ICity> {
        return await this.cityModel.findById(id);
    }

    async deleteById(id: string): Promise<void> {
        await this.cityModel.findByIdAndDelete(id);
    }
}
