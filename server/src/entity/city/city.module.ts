import { Module } from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {CitySchema} from './city.model';
import {CityService} from './city.service';
import {CityRepositroy} from './city.repository';
import {CityController} from './city.controller';


@Module({
  imports: [
    MongooseModule.forFeature([{name: 'City', schema: CitySchema}]),
  ],
  providers: [CityService, CityRepositroy],
  controllers: [CityController],
  exports: [CityService, CityRepositroy],
})
export class CityModule {}
