import { Document } from 'mongoose';

export interface ICity {
    id?: string;

    stateid?: string;

    cityname?: string;

}

export class City extends Document implements ICity {
    id: string;

    stateid: string;

    cityname: string;

}


