import {BadRequestException, Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {CityService} from './city.service';
import {CityDTO} from './city.DTO';
 import * as mongoose from 'mongoose'; 


@Controller('api')
export class CityController {

    constructor(private cityService: CityService) {
    }

    @Post('cities')
    async create(@Body() cityDTO: CityDTO): Promise<CityDTO> {
        if (cityDTO.id) {
        throw new BadRequestException('A new city cannot already have an Id');
        }
        return await this.cityService.save(cityDTO);
    }

    @Put('cities')
    async update(@Body() cityDTO: CityDTO): Promise<CityDTO> {
        if (!cityDTO.id) {
            throw new BadRequestException('A city must have an Id');
        }if (cityDTO.id && !mongoose.Types.ObjectId.isValid(cityDTO.id)) {
                throw new BadRequestException('Invalid Id.');
            }
        return await this.cityService.update(cityDTO);
    }

    @Get('cities')
    async getAll(): Promise<CityDTO[]> {
        return await this.cityService.findAll();
    }

    @Get('cities/:id')
    async getOne(@Param('id') id: string): Promise<CityDTO> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.cityService.findById(id);
    }

    @Delete('cities/:id')
    async delete(@Param('id') id: string): Promise<void> {
        if (id && !mongoose.Types.ObjectId.isValid(id)) {
            throw new BadRequestException('Invalid Id.');
        }
        return await this.cityService.deleteById(id);
    }
}
