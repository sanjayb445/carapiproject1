import {Injectable} from '@nestjs/common';
import {ICity} from './city.interface';
import {CityDTO} from './city.DTO';

@Injectable()
export class CityMapper {

  static mapCityToCityDTO(city: ICity): CityDTO {
    return {
      id: city.id,
      
      stateid: city.stateid,
      
      cityname: city.cityname,
      
    };
  }

  static mapCityDTOToCity(cityDTO: CityDTO): ICity {
    return {
      id: cityDTO.id,
      
      stateid: cityDTO.stateid,
      
      cityname: cityDTO.cityname,
      
    };
  }

  static mapCityListToCityDTOList(cityList: ICity[]): CityDTO[] {
    const cityDTOList: CityDTO[] = [];
    for (const city of cityList) {
      const cityDTO = CityMapper.mapCityToCityDTO(city);
      cityDTOList.push(cityDTO);
    }
    return cityDTOList;
  }
}
