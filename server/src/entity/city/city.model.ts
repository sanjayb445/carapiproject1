import * as mongoose from 'mongoose';

export const CitySchema = new mongoose.Schema({

    stateid: {type: mongoose.SchemaTypes.String, required: false},

    cityname: {type: mongoose.SchemaTypes.String, required: false},

});



