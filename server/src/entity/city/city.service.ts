import {CityDTO} from './city.DTO';
import {Injectable} from '@nestjs/common';
import {CityRepositroy} from './city.repository';
import {CityMapper} from './city.mapper';
import {ICity} from './city.interface';

@Injectable()
export class CityService {
constructor(private cityRepositroy: CityRepositroy) {
}

    async save(cityDTO: CityDTO): Promise<CityDTO> {
        const city: ICity = CityMapper.mapCityDTOToCity(cityDTO);
        const result = await this.cityRepositroy.save(city);
        return CityMapper.mapCityToCityDTO(result);
    }

    async update(cityDTO: CityDTO): Promise<CityDTO> {
        const city: ICity = CityMapper.mapCityDTOToCity(cityDTO);
        const result = await this.cityRepositroy.update(city);
        return CityMapper.mapCityToCityDTO(result);
    }

    async findAll(): Promise<CityDTO[]> {
        const cityList: ICity[] = await this.cityRepositroy.findAll();
        return CityMapper.mapCityListToCityDTOList(cityList);
    }

    async findById(id: string): Promise<CityDTO> {
        const city: ICity = await this.cityRepositroy.findById(id);
        return CityMapper.mapCityToCityDTO(city);
    }

    async deleteById(id: string): Promise<void> {
        return await this.cityRepositroy.deleteById(id);
    }
}
