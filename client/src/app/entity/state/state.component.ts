import {Component, OnInit} from '@angular/core';
import {StateService} from './state.service';
import {IState} from '../../shared/model/state.model';

@Component({
    selector: 'app-state',
    templateUrl: './state.component.html',
    styleUrls: []
})
export class StateComponent implements OnInit {
    states: IState[] = [];

    constructor(private stateService: StateService) {
    }

    ngOnInit() {
        this.loadAll();
    }

    trackId(index: number, item: IState) {
        return item.id;
    }

    private loadAll() {
        this.stateService.query().subscribe(res => {
            this.states = res.body;
        });
    }
}
