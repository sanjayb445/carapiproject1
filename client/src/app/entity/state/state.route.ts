import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {StateComponent} from './state.component';
import {FlareRouteGuard} from '../../shared/auth/auth.guard';
import {StateUpdateComponent} from './state-update.component';
import {Injectable} from '@angular/core';
import {IState, State} from '../../shared/model/state.model';
import {Observable, of} from 'rxjs';
import {StateService} from './state.service';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {StateDetailComponent} from './state-detail.component';
import {StateDeletePopupComponent} from './state-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class StateResolve implements Resolve<IState> {
    constructor(private service: StateService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IState> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<State>) => response.ok),
                map((state: HttpResponse<State>) => state.body)
            );
        }
        return of(new State());
    }
}

export const STATE_ROUTES: Routes = [
    {
        path: '',
        component: StateComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: 'new',
        component: StateUpdateComponent,
        resolve: {
            state: StateResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/edit',
        component: StateUpdateComponent,
        resolve: {
            state: StateResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/view',
        component: StateDetailComponent,
        resolve: {
            state: StateResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
];
export const statePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: StateDeletePopupComponent,
        resolve: {
            state: StateResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard],
        outlet: 'popup'
    }
];
