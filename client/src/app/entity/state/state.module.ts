import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StateComponent} from './state.component';
import {STATE_ROUTES, statePopupRoute} from './state.route';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {StateUpdateComponent} from './state-update.component';
import {StateDetailComponent} from './state-detail.component';
import {StateDeleteDialogComponent, StateDeletePopupComponent} from './state-delete-dialog.component';

const ENTITY_STATES = [...STATE_ROUTES, ...statePopupRoute];

@NgModule({
    declarations: [StateComponent,
      StateUpdateComponent,
      StateDetailComponent,
      StateDeleteDialogComponent,
      StateDeletePopupComponent
    ],
    entryComponents: [StateComponent,
      StateUpdateComponent,
      StateDetailComponent,
      StateDeleteDialogComponent,
      StateDeletePopupComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ENTITY_STATES),
        SharedModule,
    ]
})
export class StateModule {
}
