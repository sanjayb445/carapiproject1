import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IState} from '../../shared/model/state.model';

@Component({
  selector: 'app-state-detail',
  templateUrl: './state-detail.component.html',
  styleUrls: []
})
export class StateDetailComponent implements OnInit {
  state: IState;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ state }) => {
      this.state = state;
    });
  }

  previousState() {
    window.history.back();
  }

}
