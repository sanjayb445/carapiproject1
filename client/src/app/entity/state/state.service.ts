import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {IState} from '../../shared/model/state.model';
import {Observable} from 'rxjs';


type EntityResponseType = HttpResponse<IState>;
type EntityArrayResponseType = HttpResponse<IState[]>;

@Injectable({providedIn: 'root'})
export class StateService {
    public resourceUrl = 'api/states';

    constructor(protected http: HttpClient) {
    }

    create(state: IState): Observable<EntityResponseType> {
       return this.http
                 .post<IState>(this.resourceUrl, state , {observe: 'response'});
    }

    update(state: IState): Observable<EntityResponseType> {
        return this.http
            .put<IState>(this.resourceUrl, state , {observe: 'response'});
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<IState>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        return this.http
            .get<IState[]>(this.resourceUrl, {observe: 'response'});
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
}
