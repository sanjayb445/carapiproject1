import {Component, OnInit} from '@angular/core';
import {StateService} from './state.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {IState, State} from '../../shared/model/state.model';


@Component({
    selector: 'app-state-update',
    templateUrl: './state-update.component.html',
    styleUrls: []
})
export class StateUpdateComponent implements OnInit {
    isSaving: boolean;

    editForm = this.fb.group({
        id: [],
        statename: [],
        isleading: [],
        });

    constructor(protected stateService: StateService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({state}) => {
            this.updateForm(state);
        });
    }

    updateForm(state: IState) {
        this.editForm.patchValue({
            id: state.id,
            statename: state.statename,
            isleading: state.isleading,
            });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const state = this.createFromForm();
        if (state.id !== undefined) {
            this.subscribeToSaveResponse(this.stateService.update(state));
        } else {
            this.subscribeToSaveResponse(this.stateService.create(state));
        }
    }

    private createFromForm(): IState {
        const entity = {
             ...new State(),
             id: this.editForm.get(['id']).value || undefined,
             statename: this.editForm.get('statename').value,
             isleading: this.editForm.get('isleading').value,
             };
        return entity;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IState>>) {
        result.subscribe((res: HttpResponse<IState>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

}
