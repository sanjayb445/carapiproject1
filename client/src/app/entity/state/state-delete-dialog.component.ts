import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { StateService } from './state.service';
import {IState} from '../../shared/model/state.model';

@Component({
  selector: 'app-state-delete-dialog',
  templateUrl: './state-delete-dialog.component.html'
})
export class StateDeleteDialogComponent {
  state: IState;

  constructor(protected stateService: StateService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.stateService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

class StateDialogComponentImpl extends StateDeleteDialogComponent {
}

@Component({
  selector: 'app-state-delete-popup',
  template: ''
})
export class StateDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ state }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(StateDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.state = state;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/state', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/state', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
