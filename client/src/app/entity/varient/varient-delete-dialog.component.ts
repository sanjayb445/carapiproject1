import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { VarientService } from './varient.service';
import {IVarient} from '../../shared/model/varient.model';

@Component({
  selector: 'app-varient-delete-dialog',
  templateUrl: './varient-delete-dialog.component.html'
})
export class VarientDeleteDialogComponent {
  varient: IVarient;

  constructor(protected varientService: VarientService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.varientService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

class VarientDialogComponentImpl extends VarientDeleteDialogComponent {
}

@Component({
  selector: 'app-varient-delete-popup',
  template: ''
})
export class VarientDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ varient }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(VarientDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.varient = varient;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/varient', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/varient', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
