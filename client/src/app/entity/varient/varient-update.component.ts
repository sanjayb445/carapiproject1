import {Component, OnInit} from '@angular/core';
import {VarientService} from './varient.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {IVarient, Varient} from '../../shared/model/varient.model';
import {BrandService} from '../brand/brand.service';
import {IBrand} from '../../shared/model/brand.model';

import {CarmodelService} from '../carmodel/carmodel.service';
import {ICarmodel} from '../../shared/model/carmodel.model';

@Component({
    selector: 'app-varient-update',
    templateUrl: './varient-update.component.html',
    styleUrls: []
})
export class VarientUpdateComponent implements OnInit {
    brands: IBrand[] = [];
    carmodels: ICarmodel[] = [];
    isSaving: boolean;

    editForm = this.fb.group({
        id: [],
        brandid: [],
        modelid: [],
        carname: [],
        carimg: [],
        fueltype: [],
        transmission: [],
        price: [],
        enginecapacity: [],
        enginetype: [],
        colorurl: [],
        colorname: [],
        });

    constructor(protected varientService: VarientService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder,private brandService: BrandService,private carmodelService: CarmodelService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({varient}) => {
            this.updateForm(varient);
        });
        this.loadAll();
    }

    updateForm(varient: IVarient) {
        this.editForm.patchValue({
            id: varient.id,
            brandid: varient.brandid,
            modelid: varient.modelid,
            carname: varient.carname,
            carimg: varient.carimg,
            fueltype: varient.fueltype,
            transmission: varient.transmission,
            price: varient.price,
            enginecapacity: varient.enginecapacity,
            enginetype: varient.enginetype,
            colorurl: varient.colorurl,
            colorname: varient.colorname,
            });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const varient = this.createFromForm();
        if (varient.id !== undefined) {
            this.subscribeToSaveResponse(this.varientService.update(varient));
        } else {
            this.subscribeToSaveResponse(this.varientService.create(varient));
        }
    }

    private createFromForm(): IVarient {
        const entity = {
             ...new Varient(),
             id: this.editForm.get(['id']).value || undefined,
             brandid: this.editForm.get('brandid').value,
             modelid: this.editForm.get('modelid').value,
             carname: this.editForm.get('carname').value,
             carimg: this.editForm.get('carimg').value,
             fueltype: this.editForm.get('fueltype').value,
             transmission: this.editForm.get('transmission').value,
             price: this.editForm.get('price').value,
             enginecapacity: this.editForm.get('enginecapacity').value,
             enginetype: this.editForm.get('enginetype').value,
             colorurl: this.editForm.get('colorurl').value,
             colorname: this.editForm.get('colorname').value,
             };
        return entity;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IVarient>>) {
        result.subscribe((res: HttpResponse<IVarient>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
    trackId(index: number, item: IBrand) {
        return item.id;
    }

    private loadAll() {
        this.brandService.query().subscribe(res => {
            this.brands = res.body;
        });
        this.carmodelService.query().subscribe(res => {
            this.carmodels = res.body;
        });
    }
  

}
