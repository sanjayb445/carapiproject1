import {Component, OnInit} from '@angular/core';
import {VarientService} from './varient.service';
import {IVarient} from '../../shared/model/varient.model';

@Component({
    selector: 'app-varient',
    templateUrl: './varient.component.html',
    styleUrls: []
})
export class VarientComponent implements OnInit {
    varients: IVarient[] = [];

    constructor(private varientService: VarientService) {
    }

    ngOnInit() {
        this.loadAll();
    }

    trackId(index: number, item: IVarient) {
        return item.id;
    }

    private loadAll() {
        this.varientService.query().subscribe(res => {
            this.varients = res.body;
        });
    }
}
