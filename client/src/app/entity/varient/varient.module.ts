import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VarientComponent} from './varient.component';
import {VARIENT_ROUTES, varientPopupRoute} from './varient.route';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {VarientUpdateComponent} from './varient-update.component';
import {VarientDetailComponent} from './varient-detail.component';
import {VarientDeleteDialogComponent, VarientDeletePopupComponent} from './varient-delete-dialog.component';

const ENTITY_STATES = [...VARIENT_ROUTES, ...varientPopupRoute];

@NgModule({
    declarations: [VarientComponent,
      VarientUpdateComponent,
      VarientDetailComponent,
      VarientDeleteDialogComponent,
      VarientDeletePopupComponent
    ],
    entryComponents: [VarientComponent,
      VarientUpdateComponent,
      VarientDetailComponent,
      VarientDeleteDialogComponent,
      VarientDeletePopupComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ENTITY_STATES),
        SharedModule,
    ]
})
export class VarientModule {
}
