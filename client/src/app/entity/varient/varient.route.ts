import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {VarientComponent} from './varient.component';
import {FlareRouteGuard} from '../../shared/auth/auth.guard';
import {VarientUpdateComponent} from './varient-update.component';
import {Injectable} from '@angular/core';
import {IVarient, Varient} from '../../shared/model/varient.model';
import {Observable, of} from 'rxjs';
import {VarientService} from './varient.service';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {VarientDetailComponent} from './varient-detail.component';
import {VarientDeletePopupComponent} from './varient-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class VarientResolve implements Resolve<IVarient> {
    constructor(private service: VarientService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IVarient> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Varient>) => response.ok),
                map((varient: HttpResponse<Varient>) => varient.body)
            );
        }
        return of(new Varient());
    }
}

export const VARIENT_ROUTES: Routes = [
    {
        path: '',
        component: VarientComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: 'new',
        component: VarientUpdateComponent,
        resolve: {
            varient: VarientResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/edit',
        component: VarientUpdateComponent,
        resolve: {
            varient: VarientResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/view',
        component: VarientDetailComponent,
        resolve: {
            varient: VarientResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
];
export const varientPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: VarientDeletePopupComponent,
        resolve: {
            varient: VarientResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard],
        outlet: 'popup'
    }
];
