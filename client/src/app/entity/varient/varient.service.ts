import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {IVarient} from '../../shared/model/varient.model';
import {Observable} from 'rxjs';


type EntityResponseType = HttpResponse<IVarient>;
type EntityArrayResponseType = HttpResponse<IVarient[]>;

@Injectable({providedIn: 'root'})
export class VarientService {
    public resourceUrl = 'api/varients';

    constructor(protected http: HttpClient) {
    }

    create(varient: IVarient): Observable<EntityResponseType> {
       return this.http
                 .post<IVarient>(this.resourceUrl, varient , {observe: 'response'});
    }

    update(varient: IVarient): Observable<EntityResponseType> {
        return this.http
            .put<IVarient>(this.resourceUrl, varient , {observe: 'response'});
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<IVarient>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        return this.http
            .get<IVarient[]>(this.resourceUrl, {observe: 'response'});
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
}
