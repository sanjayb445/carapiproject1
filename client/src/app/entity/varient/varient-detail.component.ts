import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IVarient} from '../../shared/model/varient.model';

@Component({
  selector: 'app-varient-detail',
  templateUrl: './varient-detail.component.html',
  styleUrls: []
})
export class VarientDetailComponent implements OnInit {
  varient: IVarient;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ varient }) => {
      this.varient = varient;
    });
  }

  previousState() {
    window.history.back();
  }

}
