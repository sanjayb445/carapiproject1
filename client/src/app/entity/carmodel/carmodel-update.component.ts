import {Component, OnInit} from '@angular/core';
import {CarmodelService} from './carmodel.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ICarmodel, Carmodel} from '../../shared/model/carmodel.model';
import {BrandService} from '../brand/brand.service';
import {IBrand} from '../../shared/model/brand.model';

@Component({
    selector: 'app-carmodel-update',
    templateUrl: './carmodel-update.component.html',
    styleUrls: []
})
export class CarmodelUpdateComponent implements OnInit {
    isSaving: boolean;
    brands: IBrand[] = [];

    editForm = this.fb.group({
        id: [],
        brandid: [],
        modelimg: [],
        modelname: [],
        varient: [],
        });

    constructor(protected carmodelService: CarmodelService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder,private brandService: BrandService) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({carmodel}) => {
            this.updateForm(carmodel);
        });
        this.loadAll();
    }

    updateForm(carmodel: ICarmodel) {
        this.editForm.patchValue({
            id: carmodel.id,
            brandid: carmodel.brandid,
            modelimg: carmodel.modelimg,
            modelname: carmodel.modelname,
            varient: carmodel.varient,
            });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const carmodel = this.createFromForm();
        if (carmodel.id !== undefined) {
            this.subscribeToSaveResponse(this.carmodelService.update(carmodel));
        } else {
            this.subscribeToSaveResponse(this.carmodelService.create(carmodel));
        }
    }

    private createFromForm(): ICarmodel {
        const entity = {
             ...new Carmodel(),
             id: this.editForm.get(['id']).value || undefined,
             brandid: this.editForm.get('brandid').value,
             modelimg: this.editForm.get('modelimg').value,
             modelname: this.editForm.get('modelname').value,
             varient: this.editForm.get('varient').value,
             };
        return entity;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICarmodel>>) {
        result.subscribe((res: HttpResponse<ICarmodel>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
    trackId(index: number, item: IBrand) {
        return item.id;
    }

    private loadAll() {
        this.brandService.query().subscribe(res => {
            this.brands = res.body;
        });
    }

}
