import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ICarmodel} from '../../shared/model/carmodel.model';
import {Observable} from 'rxjs';


type EntityResponseType = HttpResponse<ICarmodel>;
type EntityArrayResponseType = HttpResponse<ICarmodel[]>;

@Injectable({providedIn: 'root'})
export class CarmodelService {
    public resourceUrl = 'api/carmodels';

    constructor(protected http: HttpClient) {
    }

    create(carmodel: ICarmodel): Observable<EntityResponseType> {
       return this.http
                 .post<ICarmodel>(this.resourceUrl, carmodel , {observe: 'response'});
    }

    update(carmodel: ICarmodel): Observable<EntityResponseType> {
        return this.http
            .put<ICarmodel>(this.resourceUrl, carmodel , {observe: 'response'});
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<ICarmodel>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        return this.http
            .get<ICarmodel[]>(this.resourceUrl, {observe: 'response'});
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
}
