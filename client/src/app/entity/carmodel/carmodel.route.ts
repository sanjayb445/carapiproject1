import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {CarmodelComponent} from './carmodel.component';
import {FlareRouteGuard} from '../../shared/auth/auth.guard';
import {CarmodelUpdateComponent} from './carmodel-update.component';
import {Injectable} from '@angular/core';
import {ICarmodel, Carmodel} from '../../shared/model/carmodel.model';
import {Observable, of} from 'rxjs';
import {CarmodelService} from './carmodel.service';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {CarmodelDetailComponent} from './carmodel-detail.component';
import {CarmodelDeletePopupComponent} from './carmodel-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class CarmodelResolve implements Resolve<ICarmodel> {
    constructor(private service: CarmodelService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICarmodel> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Carmodel>) => response.ok),
                map((carmodel: HttpResponse<Carmodel>) => carmodel.body)
            );
        }
        return of(new Carmodel());
    }
}

export const CARMODEL_ROUTES: Routes = [
    {
        path: '',
        component: CarmodelComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: 'new',
        component: CarmodelUpdateComponent,
        resolve: {
            carmodel: CarmodelResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/edit',
        component: CarmodelUpdateComponent,
        resolve: {
            carmodel: CarmodelResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/view',
        component: CarmodelDetailComponent,
        resolve: {
            carmodel: CarmodelResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
];
export const carmodelPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CarmodelDeletePopupComponent,
        resolve: {
            carmodel: CarmodelResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard],
        outlet: 'popup'
    }
];
