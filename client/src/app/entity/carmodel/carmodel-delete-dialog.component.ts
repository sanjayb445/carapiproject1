import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CarmodelService } from './carmodel.service';
import {ICarmodel} from '../../shared/model/carmodel.model';

@Component({
  selector: 'app-carmodel-delete-dialog',
  templateUrl: './carmodel-delete-dialog.component.html'
})
export class CarmodelDeleteDialogComponent {
  carmodel: ICarmodel;

  constructor(protected carmodelService: CarmodelService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.carmodelService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

class CarmodelDialogComponentImpl extends CarmodelDeleteDialogComponent {
}

@Component({
  selector: 'app-carmodel-delete-popup',
  template: ''
})
export class CarmodelDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ carmodel }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CarmodelDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.carmodel = carmodel;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/carmodel', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/carmodel', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
