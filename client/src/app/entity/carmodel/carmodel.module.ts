import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarmodelComponent} from './carmodel.component';
import {CARMODEL_ROUTES, carmodelPopupRoute} from './carmodel.route';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {CarmodelUpdateComponent} from './carmodel-update.component';
import {CarmodelDetailComponent} from './carmodel-detail.component';
import {CarmodelDeleteDialogComponent, CarmodelDeletePopupComponent} from './carmodel-delete-dialog.component';

const ENTITY_STATES = [...CARMODEL_ROUTES, ...carmodelPopupRoute];

@NgModule({
    declarations: [CarmodelComponent,
      CarmodelUpdateComponent,
      CarmodelDetailComponent,
      CarmodelDeleteDialogComponent,
      CarmodelDeletePopupComponent
    ],
    entryComponents: [CarmodelComponent,
      CarmodelUpdateComponent,
      CarmodelDetailComponent,
      CarmodelDeleteDialogComponent,
      CarmodelDeletePopupComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ENTITY_STATES),
        SharedModule,
    ]
})
export class CarmodelModule {
}
