import {Component, OnInit} from '@angular/core';
import {CarmodelService} from './carmodel.service';
import {ICarmodel} from '../../shared/model/carmodel.model';

@Component({
    selector: 'app-carmodel',
    templateUrl: './carmodel.component.html',
    styleUrls: []
})
export class CarmodelComponent implements OnInit {
    carmodels: ICarmodel[] = [];

    constructor(private carmodelService: CarmodelService) {
    }

    ngOnInit() {
        this.loadAll();
    }

    trackId(index: number, item: ICarmodel) {
        return item.id;
    }

    private loadAll() {
        this.carmodelService.query().subscribe(res => {
            this.carmodels = res.body;
        });
    }
}
