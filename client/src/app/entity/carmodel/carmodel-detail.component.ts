import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ICarmodel} from '../../shared/model/carmodel.model';

@Component({
  selector: 'app-carmodel-detail',
  templateUrl: './carmodel-detail.component.html',
  styleUrls: []
})
export class CarmodelDetailComponent implements OnInit {
  carmodel: ICarmodel;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ carmodel }) => {
      this.carmodel = carmodel;
    });
  }

  previousState() {
    window.history.back();
  }

}
