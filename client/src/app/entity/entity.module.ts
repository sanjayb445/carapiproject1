import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([

        {
          path: 'brand',
          loadChildren: './brand/brand.module#BrandModule'
        },

        

        {
          path: 'varient',
          loadChildren: './varient/varient.module#VarientModule'
        },

        {
          path: 'city',
          loadChildren: './city/city.module#CityModule'
        },

        {
          path: 'state',
          loadChildren: './state/state.module#StateModule'
        },

        {
          path: 'carmodel',
          loadChildren: './carmodel/carmodel.module#CarmodelModule'
        },
// Flare writing content --- flare will use it to inject modules
    ]),
  ]
})
export class EntityModule { }
