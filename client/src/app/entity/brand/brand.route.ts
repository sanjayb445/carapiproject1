import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {BrandComponent} from './brand.component';
import {FlareRouteGuard} from '../../shared/auth/auth.guard';
import {BrandUpdateComponent} from './brand-update.component';
import {Injectable} from '@angular/core';
import {IBrand, Brand} from '../../shared/model/brand.model';
import {Observable, of} from 'rxjs';
import {BrandService} from './brand.service';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {BrandDetailComponent} from './brand-detail.component';
import {BrandDeletePopupComponent} from './brand-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class BrandResolve implements Resolve<IBrand> {
    constructor(private service: BrandService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBrand> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Brand>) => response.ok),
                map((brand: HttpResponse<Brand>) => brand.body)
            );
        }
        return of(new Brand());
    }
}

export const BRAND_ROUTES: Routes = [
    {
        path: '',
        component: BrandComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: 'new',
        component: BrandUpdateComponent,
        resolve: {
            brand: BrandResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/edit',
        component: BrandUpdateComponent,
        resolve: {
            brand: BrandResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/view',
        component: BrandDetailComponent,
        resolve: {
            brand: BrandResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
];
export const brandPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BrandDeletePopupComponent,
        resolve: {
            brand: BrandResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard],
        outlet: 'popup'
    }
];
