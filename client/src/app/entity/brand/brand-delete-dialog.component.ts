import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { BrandService } from './brand.service';
import {IBrand} from '../../shared/model/brand.model';

@Component({
  selector: 'app-brand-delete-dialog',
  templateUrl: './brand-delete-dialog.component.html'
})
export class BrandDeleteDialogComponent {
  brand: IBrand;

  constructor(protected brandService: BrandService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.brandService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

class BrandDialogComponentImpl extends BrandDeleteDialogComponent {
}

@Component({
  selector: 'app-brand-delete-popup',
  template: ''
})
export class BrandDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brand }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BrandDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.brand = brand;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/brand', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/brand', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
