import {Component, OnInit} from '@angular/core';
import {BrandService} from './brand.service';
import {IBrand} from '../../shared/model/brand.model';

@Component({
    selector: 'app-brand',
    templateUrl: './brand.component.html',
    styleUrls: []
})
export class BrandComponent implements OnInit {
    brands: IBrand[] = [];

    constructor(private brandService: BrandService) {
    }

    ngOnInit() {
        this.loadAll();
    }

    trackId(index: number, item: IBrand) {
        return item.id;
    }

    private loadAll() {
        this.brandService.query().subscribe(res => {
            this.brands = res.body;
        });
    }
}
