import {Component, OnInit} from '@angular/core';
import {BrandService} from './brand.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {IBrand, Brand} from '../../shared/model/brand.model';


@Component({
    selector: 'app-brand-update',
    templateUrl: './brand-update.component.html',
    styleUrls: []
})
export class BrandUpdateComponent implements OnInit {
    isSaving: boolean;

    editForm = this.fb.group({
        id: [],
        brandllogo: [],
        brandname: [],
        });

    constructor(protected brandService: BrandService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({brand}) => {
            this.updateForm(brand);
        });
    }

    updateForm(brand: IBrand) {
        this.editForm.patchValue({
            id: brand.id,
            brandllogo: brand.brandllogo,
            brandname: brand.brandname,
            });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const brand = this.createFromForm();
        if (brand.id !== undefined) {
            this.subscribeToSaveResponse(this.brandService.update(brand));
        } else {
            this.subscribeToSaveResponse(this.brandService.create(brand));
        }
    }

    private createFromForm(): IBrand {
        const entity = {
             ...new Brand(),
             id: this.editForm.get(['id']).value || undefined,
             brandllogo: this.editForm.get('brandllogo').value,
             brandname: this.editForm.get('brandname').value,
             };
        return entity;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBrand>>) {
        result.subscribe((res: HttpResponse<IBrand>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

}
