import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IBrand} from '../../shared/model/brand.model';

@Component({
  selector: 'app-brand-detail',
  templateUrl: './brand-detail.component.html',
  styleUrls: []
})
export class BrandDetailComponent implements OnInit {
  brand: IBrand;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brand }) => {
      this.brand = brand;
    });
  }

  previousState() {
    window.history.back();
  }

}
