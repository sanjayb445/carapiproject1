import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {IBrand} from '../../shared/model/brand.model';
import {Observable} from 'rxjs';


type EntityResponseType = HttpResponse<IBrand>;
type EntityArrayResponseType = HttpResponse<IBrand[]>;

@Injectable({providedIn: 'root'})
export class BrandService {
    public resourceUrl = 'api/brands';

    constructor(protected http: HttpClient) {
    }

    create(brand: IBrand): Observable<EntityResponseType> {
       return this.http
                 .post<IBrand>(this.resourceUrl, brand , {observe: 'response'});
    }

    update(brand: IBrand): Observable<EntityResponseType> {
        return this.http
            .put<IBrand>(this.resourceUrl, brand , {observe: 'response'});
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<IBrand>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        return this.http
            .get<IBrand[]>(this.resourceUrl, {observe: 'response'});
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
    getAllBrand(brandid: string): Observable<EntityResponseType> {
        return this.http
            .get<IBrand>(`${this.resourceUrl}/brand/${brandid}`, {observe: 'response'});
    }
}
