import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrandComponent} from './brand.component';
import {BRAND_ROUTES, brandPopupRoute} from './brand.route';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {BrandUpdateComponent} from './brand-update.component';
import {BrandDetailComponent} from './brand-detail.component';
import {BrandDeleteDialogComponent, BrandDeletePopupComponent} from './brand-delete-dialog.component';

const ENTITY_STATES = [...BRAND_ROUTES, ...brandPopupRoute];

@NgModule({
    declarations: [BrandComponent,
      BrandUpdateComponent,
      BrandDetailComponent,
      BrandDeleteDialogComponent,
      BrandDeletePopupComponent
    ],
    entryComponents: [BrandComponent,
      BrandUpdateComponent,
      BrandDetailComponent,
      BrandDeleteDialogComponent,
      BrandDeletePopupComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ENTITY_STATES),
        SharedModule,
    ]
})
export class BrandModule {
}
