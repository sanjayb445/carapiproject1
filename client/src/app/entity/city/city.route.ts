import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {CityComponent} from './city.component';
import {FlareRouteGuard} from '../../shared/auth/auth.guard';
import {CityUpdateComponent} from './city-update.component';
import {Injectable} from '@angular/core';
import {ICity, City} from '../../shared/model/city.model';
import {Observable, of} from 'rxjs';
import {CityService} from './city.service';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {CityDetailComponent} from './city-detail.component';
import {CityDeletePopupComponent} from './city-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class CityResolve implements Resolve<ICity> {
    constructor(private service: CityService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICity> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<City>) => response.ok),
                map((city: HttpResponse<City>) => city.body)
            );
        }
        return of(new City());
    }
}

export const CITY_ROUTES: Routes = [
    {
        path: '',
        component: CityComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: 'new',
        component: CityUpdateComponent,
        resolve: {
            city: CityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/edit',
        component: CityUpdateComponent,
        resolve: {
            city: CityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
    {
        path: ':id/view',
        component: CityDetailComponent,
        resolve: {
            city: CityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard]
    },
];
export const cityPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CityDeletePopupComponent,
        resolve: {
            city: CityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [FlareRouteGuard],
        outlet: 'popup'
    }
];
