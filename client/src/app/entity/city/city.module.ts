import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CityComponent} from './city.component';
import {CITY_ROUTES, cityPopupRoute} from './city.route';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {CityUpdateComponent} from './city-update.component';
import {CityDetailComponent} from './city-detail.component';
import {CityDeleteDialogComponent, CityDeletePopupComponent} from './city-delete-dialog.component';

const ENTITY_STATES = [...CITY_ROUTES, ...cityPopupRoute];

@NgModule({
    declarations: [CityComponent,
      CityUpdateComponent,
      CityDetailComponent,
      CityDeleteDialogComponent,
      CityDeletePopupComponent
    ],
    entryComponents: [CityComponent,
      CityUpdateComponent,
      CityDetailComponent,
      CityDeleteDialogComponent,
      CityDeletePopupComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ENTITY_STATES),
        SharedModule,
    ]
})
export class CityModule {
}
