import {Component, OnInit} from '@angular/core';
import {CityService} from './city.service';
import {ICity} from '../../shared/model/city.model';

@Component({
    selector: 'app-city',
    templateUrl: './city.component.html',
    styleUrls: []
})
export class CityComponent implements OnInit {
    cities: ICity[] = [];

    constructor(private cityService: CityService) {
    }

    ngOnInit() {
        this.loadAll();
    }

    trackId(index: number, item: ICity) {
        return item.id;
    }

    private loadAll() {
        this.cityService.query().subscribe(res => {
            this.cities = res.body;
        });
    }
}
