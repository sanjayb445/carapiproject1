import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {ICity} from '../../shared/model/city.model';
import {Observable} from 'rxjs';


type EntityResponseType = HttpResponse<ICity>;
type EntityArrayResponseType = HttpResponse<ICity[]>;

@Injectable({providedIn: 'root'})
export class CityService {
    public resourceUrl = 'api/cities';

    constructor(protected http: HttpClient) {
    }

    create(city: ICity): Observable<EntityResponseType> {
       return this.http
                 .post<ICity>(this.resourceUrl, city , {observe: 'response'});
    }

    update(city: ICity): Observable<EntityResponseType> {
        return this.http
            .put<ICity>(this.resourceUrl, city , {observe: 'response'});
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<ICity>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        return this.http
            .get<ICity[]>(this.resourceUrl, {observe: 'response'});
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
}
