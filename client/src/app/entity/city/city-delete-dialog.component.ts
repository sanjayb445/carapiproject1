import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CityService } from './city.service';
import {ICity} from '../../shared/model/city.model';

@Component({
  selector: 'app-city-delete-dialog',
  templateUrl: './city-delete-dialog.component.html'
})
export class CityDeleteDialogComponent {
  city: ICity;

  constructor(protected cityService: CityService, public activeModal: NgbActiveModal) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: string) {
    this.cityService.delete(id).subscribe(response => {
      this.activeModal.dismiss(true);
    });
  }
}

class CityDialogComponentImpl extends CityDeleteDialogComponent {
}

@Component({
  selector: 'app-city-delete-popup',
  template: ''
})
export class CityDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ city }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CityDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.city = city;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/city', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/city', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
