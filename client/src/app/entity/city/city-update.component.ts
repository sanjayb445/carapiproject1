import {Component, OnInit} from '@angular/core';
import {CityService} from './city.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ICity, City} from '../../shared/model/city.model';


@Component({
    selector: 'app-city-update',
    templateUrl: './city-update.component.html',
    styleUrls: []
})
export class CityUpdateComponent implements OnInit {
    isSaving: boolean;

    editForm = this.fb.group({
        id: [],
        stateid: [],
        cityname: [],
        });

    constructor(protected cityService: CityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({city}) => {
            this.updateForm(city);
        });
    }

    updateForm(city: ICity) {
        this.editForm.patchValue({
            id: city.id,
            stateid: city.stateid,
            cityname: city.cityname,
            });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        const city = this.createFromForm();
        if (city.id !== undefined) {
            this.subscribeToSaveResponse(this.cityService.update(city));
        } else {
            this.subscribeToSaveResponse(this.cityService.create(city));
        }
    }

    private createFromForm(): ICity {
        const entity = {
             ...new City(),
             id: this.editForm.get(['id']).value || undefined,
             stateid: this.editForm.get('stateid').value,
             cityname: this.editForm.get('cityname').value,
             };
        return entity;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICity>>) {
        result.subscribe((res: HttpResponse<ICity>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

}
