
export interface ICity {
  id?: string;
  stateid?: string;
  cityname?: string;
  }

export class City implements ICity {
  constructor(
   public id?: string,
   public stateid?: string,
   public cityname?: string,
  ) {}
}


