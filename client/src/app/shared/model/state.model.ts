
export interface IState {
  id?: string;
  statename?: string;
  isleading?: string;
  }

export class State implements IState {
  constructor(
   public id?: string,
   public statename?: string,
   public isleading?: string,
  ) {}
}


