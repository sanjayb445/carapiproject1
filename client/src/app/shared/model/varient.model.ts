
export interface IVarient {
  id?: string;
  brandid?: string;
  modelid?: string;
  carname?: string;
  carimg?: string;
  fueltype?: string;
  transmission?: string;
  price?: string;
  enginecapacity?: string;
  enginetype?: string;
  colorurl?: string;
  colorname?: string;
  }

export class Varient implements IVarient {
  constructor(
   public id?: string,
   public brandid?: string,
   public modelid?: string,
   public carname?: string,
   public carimg?: string,
   public fueltype?: string,
   public transmission?: string,
   public price?: string,
   public enginecapacity?: string,
   public enginetype?: string,
   public colorurl?: string,
   public colorname?: string,
  ) {}
}


