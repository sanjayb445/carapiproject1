
export interface ICarmodel {
  id?: string;
  brandid?: string;
  modelimg?: string;
  modelname?: string;
  varient?: string;
  }

export class Carmodel implements ICarmodel {
  constructor(
   public id?: string,
   public brandid?: string,
   public modelimg?: string,
   public modelname?: string,
   public varient?: string,
  ) {}
}


