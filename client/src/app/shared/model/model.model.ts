
export interface IModel {
  id?: string;
  brandid?: string;
  modelname?: string;
  mdoelimg?: string;
  varient?: string;
  }

export class Model implements IModel {
  constructor(
   public id?: string,
   public brandid?: string,
   public modelname?: string,
   public mdoelimg?: string,
   public varient?: string,
  ) {}
}


