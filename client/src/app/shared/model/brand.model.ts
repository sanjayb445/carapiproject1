
export interface IBrand {
  id?: string;
  brandllogo?: string;
  brandname?: string;
  }

export class Brand implements IBrand {
  constructor(
   public id?: string,
   public brandllogo?: string,
   public brandname?: string,
  ) {}
}


